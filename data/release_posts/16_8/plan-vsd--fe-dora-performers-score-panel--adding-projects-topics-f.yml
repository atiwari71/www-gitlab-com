---
features:
  primary:
  - name: New organization-level DevOps view with DORA-based industry benchmarks
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html#dora-performers-score-panel'
    image_url: '/images/16_8/16.8_vsd_dora.png'
    reporter: hsnir1
    stage: plan
    categories:
        - Value Stream Management
        - DORA Metrics
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/426516'
    description: |
     We added a new **DORA Performers score** panel to the [Value Streams Dashboard](https://www.youtube.com/watch?v=EA9Sbks27g4) to visualize the status of the organization's DevOps performance across different projects. This new visualization displays a breakdown of the DORA score (high, medium, or low) so that executives can understand the organization's DevOps health top to bottom.

     The [four DORA metrics](https://about.gitlab.com/solutions/value-stream-management/dora/#overview) are available out-of-the-box in GitLab, and now with the new DORA scores organizations can compare their DevOps performance against [industry benchmarks](https://dora.dev/) or peers. This benchmarking helps executives understand where they stand in relation to others, and identify best practices or areas where they might be lagging behind.

     To help us improve the Value Streams Dashboard, please share feedback about your experience in this [survey](https://gitlab.fra1.qualtrics.com/jfe/form/SV_50guMGNU2HhLeT4).
