---
features:
  primary:
  - name: "Security policy scopes"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/policies/scan-execution-policies.html#security-policy-scopes'
    image_url: '/images/16_11/policy-scoping-release-post-image-optimized.png'
    reporter: g.hickman
    stage: govern
    categories:
    - Security Policy Management
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/5510'
    description: |
      Policy scoping provides granular management and enforcement of policies. Across both merge request approval (scan result) policies and scan execution policies, this new feature enables security and compliance teams to scope policy enforcement to a compliance framework or to a set of included/excluded projects in a group.

      While today all policies managed in a security policy project are enforced against all linked groups, subgroups, and projects, policy scoping will allow you to refine that enforcement policy by policy. This allows security and compliance teams to:

      - More easily manage policies centrally across their organization, while still enforcing policies granularly.
      - Get a better sense of how the controls they are implementing and enforcing in GitLab roll up to the compliance frameworks they've defined.
      - View and manage which policies are linked to a compliance framework through the compliance center.
      - Better organize and understand their security and compliance posture.
