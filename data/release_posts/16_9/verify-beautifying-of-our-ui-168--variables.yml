---
features:
  primary:
  - name: "Improvements to the CI/CD variables user interface"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/ci/variables/'
    video: 'https://www.youtube-nocookie.com/embed/gdL2cEp3kw0?si=WgWO8ZB8CAGWPajM'
    reporter: jocelynjane
    stage: verify
    categories:
    - Secrets Management
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/418331'
    description: |
      In GitLab 16.9, we have released a series of improvements to the CI/CD variables user experience. We have improved the variables creation flow through changes including:

      - [Improved validation when variable values do not meet the requirements](https://gitlab.com/gitlab-org/gitlab/-/issues/365934).
      - [Help text during variable creation](https://gitlab.com/gitlab-org/gitlab/-/issues/410220).
      - [Allow resizing of the value field in the variables form](https://gitlab.com/gitlab-org/gitlab/-/issues/434667).

      Other improvements include a new, [optional description field for group and project variables](https://gitlab.com/gitlab-org/gitlab/-/issues/378938) to assist with the management of variables. We have also made it easier to [add or edit multiple variables](https://gitlab.com/gitlab-org/gitlab/-/issues/434666), lowering the friction in the software development workflow and enabling developers to perform their job more efficiently.

      Your [feedback for these changes](https://gitlab.com/gitlab-org/gitlab/-/issues/441177) is always valued and appreciated.
